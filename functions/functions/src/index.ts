import * as functions from 'firebase-functions';
import * as admin  from "firebase-admin";
admin.initializeApp();
// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript
// Listen for updates to any `user` document.
exports.nuevoReferido = functions.firestore
    .document('users/{userId}')
    .onCreate((change, context) => {
        // return null; 
      // Retrieve the current and previous value
      const data = change.data();
      // Then return a promise of a set operation to update the count
      change.ref.parent?.doc(data.referalemail).get().then(function(doc) {
        if (doc.exists) {
      // Retrieve the current count of name changes
    //   const datos = doc.data();
      let count = doc.get('contador_referidos');
            if (!count) {
                count = 0;
              }
              return change.ref.parent?.doc(data.referalemail).update({contador_referidos: count + 1});
          console.log("Document data:", doc.data());
        } else {
          // doc.data() will be undefined in this case
          return null;
          console.log("No such document!");
        }
      }).catch(function(error) {
        console.log("Error getting document:", error);
        return null;
      });
    //   const previousData = change.before.data();

    //   // We'll only update if the name has changed.
    //   // This is crucial to prevent infinite loops.

    //   if (data.name === previousData.name) return null;

    //   // Retrieve the current count of name changes
    //   let count = data.ContadorReferidos;
    //   if (!count) {
    //     count = 0;
    //   }

    //   // Then return a promise of a set operation to update the count
    //   return change.ref.set({
    //     name_change_count: count + 1
    //   }, {merge: true});
    });

    exports.actualizarPuntos = functions.firestore
    .document('users/{userId}')
    .onUpdate(async (change, context) => {
      // Get an object representing the document
      // e.g. {'name': 'Marie', 'age': 66}
      const oldValue = change.before.data()
      const newValue = change.after.data();

      const contador_referidoss = newValue.contador_referidos;
      const puntos = 25;
      let aux_coinSoy_VIP= newValue.coin_SoyVIP;

      if(newValue.coin_SoyVIP>oldValue.coin_SoyVIP){
        const registrationToken = newValue.token;
        enviarNotificacion(registrationToken,'Pago recibido','Has recibido ' + (newValue.coin_SoyVIP - oldValue.coin_SoyVIP) + '$');
      }
        if (!aux_coinSoy_VIP) {
          aux_coinSoy_VIP = 0;
        }
      if(contador_referidoss>=5){
        enviarNotificacion('duvb1S7ApnM:APA91bHm649nbunwGckYg2Ry7UyHRA9YqUAWONsVYgkOpaREbJEga2j_76ho4O_dkU7QiCessIdHhK1Gb9y9FVXwxVo2fyq2709YWG6hLxZO9JcbpKFDVrxSrAR7AEHujdoI9gMyYp12','Alerta',newValue.info[0] + ' ha recibido ' + 25 + '$');
        enviarNotificacion('f1yLx3-vfRE:APA91bHKGgbzSR_wPfzMTSp3ayD2dXjAyPf7S9-EcY2z6q5C66Y2xVlsTKQREvt2TLGU6myXgRTWrs8NXbX44aMRgRndjBE_lcqnSe6kmLyRDCnh3PzRLWN6WWFqQQMpOQycadrq6HEa','Alerta',newValue.info[0] + ' ha recibido ' + 25 + '$');
        return change.after.ref.set({
            contador_referidos: 0,
            coin_SoyVIP: aux_coinSoy_VIP + puntos,
            //TODO:Crear Variable de dinero
              }, {merge: true});
      }else{

        return null;
      }

      // perform desired operations ...
    });
    function enviarNotificacion(tokenDispositivo: string, titulo: string, cuerpo: string){

      const message = {
        // data: {
        //   score: '850',
        //   time: '2:45'
        // },
        notification: {
        title: titulo,
        body: cuerpo,
        },
        token: tokenDispositivo
      };
      admin.messaging().send(message)
      .then((response) => {
        // Response is a message ID string.
        console.log('Successfully sent message:', response);
      })
      .catch((error) => {
        console.log('Error sending message:', error);
      });
     
    }