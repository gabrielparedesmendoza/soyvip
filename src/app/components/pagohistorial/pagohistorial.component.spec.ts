import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagohistorialComponent } from './pagohistorial.component';

describe('PagohistorialComponent', () => {
  let component: PagohistorialComponent;
  let fixture: ComponentFixture<PagohistorialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagohistorialComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagohistorialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
