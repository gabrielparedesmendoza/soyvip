import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { BrandsService } from '../../services/brands.service';

@Component({
  selector: 'app-pagohistorial',
  templateUrl: './pagohistorial.component.html',
  styleUrls: ['./pagohistorial.component.scss'],
})
export class PagohistorialComponent implements OnInit {0
  ciudadNueva = '';
  constructor(
    private brandservice: BrandsService,
    private popoverCtrl: PopoverController,
  ) { }


  ngOnInit() {
  }
agregar(){
  if(this.ciudadNueva === ''){
    alert('Error');
  } else {
  this.brandservice.agregarCiudad(this.ciudadNueva);
  alert('Se agrego una nueva ciudad');
  this.DismissClick();
  }
}
async DismissClick() {
  await this.popoverCtrl.dismiss();
    }
}
