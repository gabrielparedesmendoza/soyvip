import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertController, PopoverController } from '@ionic/angular';
import { BrandsService} from '../../services/brands.service';

@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.scss'],
})
export class PagoComponent implements OnInit {
  pagarID: string;
  pagarCantidad: number;
  scannedData: {};
  verificarid: string;
  imagenpaago = undefined;

  public infouserid2: any = [];
  public infouserid: any = [];
  infouser: any;
  flagPago= true;
  constructor(
    private barcodeScanner: BarcodeScanner,
    private brandservice: BrandsService,
    private popoverCtrl: PopoverController,
    public alertController: AlertController,
  ) { }

  ngOnInit() {}
  scanBar() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        this.pagarID = barcodeData.text;
        // alert("Barcode data " + JSON.stringify(barcodeData));
        this.onVerifyID();
        this.scannedData = barcodeData;
      })
      .catch(err => {
        console.log("Error", err);
      });
  }
  async onVerifyID() {
    // this.pagarID = "SoyVIP";
    this.pagarID = (this.pagarID.replace(' ', '')).toUpperCase();

    if (this.pagarID === '') {
      // this.authSvc.presentAlert("Ingrese el codigo de su parthner");
    } else {

      this.brandservice.getUserInfopartner().subscribe(info => {

        this.infouserid2 = info;

        for (let i = 0; i < info.length; i++) {

          if (this.pagarID === this.infouserid2[i].idSoyVIP) {
            this.infouserid[0] = this.infouserid2[i];
            if ( this.infouserid[0].coin_SoyVIP === undefined) {
              this.infouserid[0].coin_SoyVIP = 0;
            }
            this.verificarid = '1';
            break;
          } else {
            this.verificarid = '0';
          }
        }


        switch (this.verificarid) {
          case '0':
            alert('Código de socio no valido');
            break;
          case '1':
            // alert('Esta cuenta pertenece a:  ' + this.infouserid[0].info[0]);
            break;
        }

      });
    }

  }
  async presentAlert(mensaje: string) {
    const alert = await this.alertController.create({
      //header: 'Error',
      //subHeader: 'Informacion',
      message: mensaje,
      buttons: ['OK']
    });

    await alert.present();
  }
  pagar() {
    console.log(this.infouser[0].coin_SoyVIP);
    if (this.infouser[0].coin_SoyVIP >= this.pagarCantidad && this.infouserid[0].idSoyVIP !== this.infouser[0].idSoyVIP) {
      this.flagPago = false;
      this.brandservice.pagar({ paga: this.infouser[0], cobra: this.infouserid[0], cantidad: this.pagarCantidad });
      this.imagenpaago = 'assets/icon/Pago/001.jpg';
      this.presentAlert('Pago exitoso');
    } else {
      this.imagenpaago = 'assets/icon/Pago/002.jpg';
      this.presentAlert('Error de pago');
    }

  }
  async DismissClick() {
    await this.popoverCtrl.dismiss();
      }
}

