import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from "@ionic/angular";
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss'],
})
export class BrandComponent implements OnInit {
public chat : any;
brand:any;
  constructor(
    private navparams:NavParams, 
    private modal:ModalController,
    private authSvc: AuthService
    ) { }

  ngOnInit( ) {
    this.chat = this.navparams.get('brand.name');
    this.authSvc.onClose('brand');
  }
  closeBrand() {
        this.modal.dismiss();
  }

}
