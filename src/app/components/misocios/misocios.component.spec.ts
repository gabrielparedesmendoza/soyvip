import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisociosComponent } from './misocios.component';

describe('MisociosComponent', () => {
  let component: MisociosComponent;
  let fixture: ComponentFixture<MisociosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisociosComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisociosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
