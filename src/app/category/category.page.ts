import { Component, OnInit } from '@angular/core';
import { AuthService } from "../services/auth.service";
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { User } from "../shared/user.class";
import { NavController, MenuController } from '@ionic/angular';
import { delay } from 'q';
import { BrandsService, fuser } from '../services/brands.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})

export class CategoryPage implements OnInit {
  offset = 0;
  public Publicidad: any = [];
  user: User = new User;
  ciudades: [];
  ciudadesJSON: any[] = [] ;
  ciudadSelecionada = window.localStorage.ciudad;
  customActionSheetOptions: any = {
    header: 'Ciudad',
    subHeader: 'Seleccione una ciudad'
  };
  menu: any;
  public infouser: any = [];

  constructor(
    private router: Router,
    private authSvc: AuthService,
    public nav: NavController,
    private menuCtrl: MenuController,
    private brandservice: BrandsService
  ) {
    this.user.email = window.localStorage['email'];
    this.user.hasVerifiedEmail = true;
    this.authSvc.onVerification(this.user);
  }

  async ngOnInit() {
    this.encontrarPublicidad();
    this.encontrarCiudades();
    this.findUserInfo();
    this.authSvc.onClose("category");
  }
  onPost() {

  }
  async encontrarPublicidad() {
    this.brandservice.getPublicidad().subscribe( publicidad => {
          this.Publicidad = publicidad;
      }
      );
  }
  async encontrarCiudades() {
    if (this.ciudadSelecionada === undefined) {
      this.ciudadSelecionada = 'Manta';
    }
    // tslint:disable-next-line: variable-name
    this.brandservice.getCiudad().subscribe( a_global => {
          this.ciudades = a_global.ciudades;
          // console.log(this.ciudades);
          for (const item of this.ciudades) {
            const aux = {
              nombre: item,
            };
            this.ciudadesJSON.push(aux);
          }
          // console.log(this.ciudadesJSON);
        }
      );

    }
  async actualizarToken() {
    if (this.infouser[0].token !== window.localStorage.token) {
      this.infouser[0].token = window.localStorage.token;
      this.brandservice.actualizarToken(this.infouser[0]);
    }
    }
  async findUserInfo() {
    const asdsa: fuser = {
      active: false,
      beneficiario: [],
      // tslint:disable-next-line: new-parens
      date: new Date,
      id: '',
      email: '',
      image: '',
      info: [],
      referal: '',
      sendimg: false,
      users: [],
      idSoyVIP: '',
      admin : false,
      referalemail: '',
      coin_SoyVIP : 0,
      contador_referidos: 0,
      coin_Historial: [' '],
      token : '',
        }
    this.infouser[0] = asdsa;
    this.authSvc.onVerification(this.user);
    await delay(500);
    this.brandservice.getUserInfo(this.user.email).subscribe(info => {
      this.infouser = info;
      if (this.infouser[0] == null) {
        const asdsa: fuser = {
          active: false,
          beneficiario: [],
          date: new Date,
          id: "",
          email: "",
          image: "",
          info: [],
          referal: "",
          sendimg:false,
          users: [],
          idSoyVIP:"",
          admin : false,
          referalemail: '',
          coin_SoyVIP: 0,
          contador_referidos: 0,
          coin_Historial: [' '],
          token: '',
        };
      }
      this.actualizarToken();
      // console.log(this.infouser[0].active);
    });
  }
  selectMenu1() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category01";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu2() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category02";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu3() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category03";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu4() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category04";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu5() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category05";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu6() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category06";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu7() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category07";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu8() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category08";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu9() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category09";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu10() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category10";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu11() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category11";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu12() {
    window.localStorage.ciudad = this.ciudadSelecionada;
    this.authSvc.onClose("");
    const menu = this.menu = "category12";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
}
