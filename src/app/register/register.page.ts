import { Component, OnInit } from '@angular/core';
//import { User } from 'src/models/user';
import { User } from "../shared/user.class";
//import { AngularFireAuth } from "@angular/fire/auth";
import { AuthService } from "../services/auth.service";
import { Router } from "@angular/router";
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})

export class RegisterPage implements OnInit {

  user:User=new User();
  
  ts:boolean;
  cuser:String;
  constructor(private router:Router,
    private authSvc:AuthService) { }

  ngOnInit() {
    this.authSvc.onClose('');
  }
  
  async onRegister(){
    if(this.user.password==this.cuser){
      if(this.ts==true){
        this.user.email=this.user.email.replace(' ', '');
        const user = await this.authSvc.onRegister(this.user);
      if(user){
        
        console.log("Register is succesfull");
  
        this.authSvc.onSendVerificationEmail(this.user);
        this.authSvc.presentAlert("Por favor revisa tu correo electrónico y confirma tu cuenta");
        this.router.navigateByUrl('./taps/category');
      }  
      }else{
        this.authSvc.presentAlert('Por favor acepte los terminos y condiciones');
      }
    }else
    {
      this.authSvc.presentAlert('Su contraseña no coinciden');
    }
    
    
  }



//  async register(user:User){
//   try {
//     const result= await this.afAuth.auth.createUserWithEmailAndPassword(user.email,user.password);
//     console.log(result);
//    } catch (error) {
//      console.error(error);
//    }


//   }
}
