import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from "./guards/auth.guard";
import { Auth2Guard } from "./guards/auth2.guard";
const routes: Routes = [
   { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule), canActivate:[AuthGuard]},
  { path: 'login', loadChildren: './login/login.module#LoginPageModule',canActivate:[Auth2Guard]},
  { path: 'newpass', loadChildren: './newpass/newpass.module#NewpassPageModule',canActivate:[Auth2Guard]},
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule',canActivate:[Auth2Guard]},
  // { path: 'category', loadChildren: './category/category.module#CategoryPageModule' },
  { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
  // { path: 'user', loadChildren: './user/user.module#UserPageModule' },
  { path: 'terms', loadChildren: './terms/terms.module#TermsPageModule' },
  { path: 'intro', loadChildren: './intro/intro.module#IntroPageModule' },
  { path: 'detalles/:id', loadChildren: './detalles/detalles.module#DetallesPageModule' },
  { path: 'detalles', loadChildren: './detalles/detalles.module#DetallesPageModule' },
  { path: 'administrador', loadChildren: './administrador/administrador.module#AdministradorPageModule' },
  { path: 'pagohistorial', loadChildren: './pagohistorial/pagohistorial.module#PagohistorialPageModule' },
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule'},
  //,canActivate:[AuthGuard]},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
