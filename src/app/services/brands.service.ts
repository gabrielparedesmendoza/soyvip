import { Injectable } from '@angular/core';
import { map, finalize } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import { firestore } from 'firebase';


// tslint:disable-next-line: class-name
export interface brand {
  description: string;
  name: string;
  id: string;
  image: string;
  activity1: any;
  activity2: any;
  activity3: any;
  activity4: any;
  activity5: any;
  info: any;
}
// tslint:disable-next-line: class-name
export interface fuser {
  active: boolean;
  beneficiario: any;
  date: Date;
  id: string;
  email: string;
  image: string;
  info: any;
  referal: string;
  referalemail: string;
  sendimg: boolean;
  users: any;
  idSoyVIP: any;
  admin: boolean;
  coin_SoyVIP: number;
  coin_Historial: any;
  contador_referidos: number;
  token: string;

}
// tslint:disable-next-line: class-name
export interface data {
  active: string;
  date: string;
  id: string;
  users: string;
  referal: string;
}
// tslint:disable-next-line: class-name
export interface datapublicidad {
  id: string;
  url: string;
}
export interface a_global {
  ciudades: [];
  id: string;
}

@Injectable({
  providedIn: 'root'
})
export class BrandsService {
  public uploadPercent: Observable<number>;
  public downloadUrl: Observable<any>;
  public Marcas: any = [];
  constructor(private aFS: AngularFirestore, private afStorage: AngularFireStorage) { }
  getInf(category: string) {
    return this.aFS.collection(category , ref =>
      ref
        .where('info', 'array-contains', window.localStorage.ciudad)
        ).snapshotChanges().pipe(map(rbrands => {
      return rbrands.map(a => {
        // tslint:disable-next-line: no-shadowed-variable
        const data = a.payload.doc.data() as brand;
        data.id = a.payload.doc.id;
        // console.log(data.id);
        // console.log( window.localStorage.ciudad);
        return data;
      });
    }));
  }
  getInf2(category: string) {
    return this.aFS.collection(category).snapshotChanges().pipe(map(rbrands => {
      return rbrands.map(a => {
        // tslint:disable-next-line: no-shadowed-variable
        const data = a.payload.doc.data() as brand;
        data.id = a.payload.doc.id;
        // console.log(data.id);
        return data;
      });
    }));
  }

  getBrand(id: string) {
    // tslint:disable-next-line: prefer-const
    let idA = id.split('/');
    return this.aFS.collection(idA[0]).snapshotChanges().pipe(map(rbrands => {
      return rbrands.map(a => {
        // tslint:disable-next-line: no-shadowed-variable
        const data = a.payload.doc.data() as brand;
        data.id = a.payload.doc.id;
        if (a.payload.doc.id === idA[1]) {
          // console.log(data);
          return data;
        }
      });
    }));
  }
  getInfAdmin(category: string) {

    return this.aFS.collection(category).snapshotChanges().pipe(map(rbrands => {
      return rbrands.map(a => {
        // tslint:disable-next-line: no-shadowed-variable
        const data = a.payload.doc.data() as brand;
        data.id = category + '/' + a.payload.doc.id;
        // console.log(data.id);
        return data;
      });
    }));
  }
  async agregarMarca(marca: any, category: string) {
    alert('Se esta agregando marca' + marca.name);
    return this.aFS.collection(category).add(marca).then(newItem => {
      console.log('ID:' + newItem.id);
    }
    ).catch(err => console.log(err));
    // return this.aFS.collection('category02').doc('brandgp').set(marca);
    // return this.aFS.collection('category01/').doc('brandgp').set(marca);
    // return this.aFS.collection('category01/brandgp').add(marca);
  }
  eliminarImagen(actividad: string, category: string, marca: any) {
    // this.aFS.collection(category).doc(marca).update(
    //   {
    //     actividad: firestore.FieldValue.(ciudad),
    //   });
    }
 async actualizarMarca(category: string, id: string, marca: any) {
  alert('Se esta actualizando marca' + marca.name);
  return this.aFS.collection(category).doc(id).update(marca);
 }
 async actualizarPublicidad(publicidad: any) {
  // tslint:disable-next-line: prefer-for-of
  for (let i = 0; i < publicidad.length; i++) {
    const element = publicidad[i];
    const Publicidad = {
      url : element,
  };
    if (i < 10) {
      this.aFS.collection('publicidad').doc('Publicidad0' + i).update(Publicidad);
    } else {
      this.aFS.collection('publicidad').doc('Publicidad10').update(Publicidad);
    }
  }
  return true;

//  return this.aFS.collection('publicidad').doc('Publicidad00').update('{"url":' + publicidad[0] + '}');
}
  eliminarMarca(category: string, id: string) {
    return this.aFS.collection(category).doc(id).delete();
    // return this.aFS.doc(id).delete();
  }

  getPublicidad() {
    return this.aFS.collection('publicidad').snapshotChanges().pipe(map(rbrands => {
      return rbrands.map(a => {
        // tslint:disable-next-line: no-shadowed-variable
        const data = a.payload.doc.data() as datapublicidad;
        data.id = a.payload.doc.id;
        // console.log(data.id);
        // console.log(data.url);
        return data;
      });
    }));
  }
  getCiudad() {
    return this.aFS.collection('users').doc('a_globales').snapshotChanges().pipe(map(a => {
        // tslint:disable-next-line: no-shadowed-variable
        const data = a.payload.data() as a_global;
        // console.log(data);
        // console.log(data.url);
        return data;
    }));
  }

  // tslint:disable-next-line: variable-name
  getUserInfo(user_id: string) {
    // set(Token);
    return this.aFS.collection('users').doc(user_id).snapshotChanges().pipe(map(a => {
      // tslint:disable-next-line: no-shadowed-variable
      const data = a.payload.data() as data;
      // console.log('Informacion Usuario',[data]);
      return [data];
    })
    );
  }
  actualizarToken(usuario: any) {
    return this.aFS.collection('users').doc(usuario.email).set(usuario);
  }
  getUserInfopartner() {
    return this.aFS.collection('users').snapshotChanges().pipe(map(rbrands => {
      return rbrands.map(a => {
        // tslint:disable-next-line: no-shadowed-variable
        const data = a.payload.doc.data() as brand;
        // console.log(data);
        return data;
      });
    }));

}

// tslint:disable-next-line: variable-name
activeSocios(id_f: string) {
  return this.aFS.collection('users').doc(id_f).update(
    {active: true, }
  );
}
pagar({ paga, cobra, cantidad }: { paga: fuser; cobra: fuser; cantidad: number; }) {
this.aFS.collection('users').doc(paga.email).update(
  {
  coin_SoyVIP: paga.coin_SoyVIP - cantidad ,
  coin_Historial: firestore.FieldValue.
        arrayUnion(new Date().toLocaleString() + ',' + cobra.idSoyVIP + ',Egreso,' + cantidad),
  }
);
this.aFS.collection('users').doc(cobra.email).update(
  {
    coin_SoyVIP: cobra.coin_SoyVIP + cantidad ,
    coin_Historial: firestore.FieldValue.
    arrayUnion(new Date().toLocaleString() + ',' + cobra.idSoyVIP + ',Ingreso,' + cantidad),
  }
);
console.log(paga.email + ' cobra' + cobra.email);


}

agregarCiudad(ciudad: string) {
  this.aFS.collection('users').doc('a_globales').update(
    {
    ciudades: firestore.FieldValue.
          arrayUnion(ciudad),
    });
  }
  

  // tslint:disable-next-line: no-shadowed-variable variable-name
  uploadInfoUsertoFirebase(id_f: string, fuser: fuser) {
    return this.aFS.collection('users').doc(id_f).set(fuser);
    // return this.aFS.collection('users').add(fuser);
  }

  // tslint:disable-next-line: no-shadowed-variable variable-name
  uploadReferal(id_f: string, fuser: string) {
    return this.aFS.collection('users').doc(id_f).update(
      {users: firestore.FieldValue.
        arrayUnion(fuser), }
    );
  }
  // tslint:disable-next-line: no-shadowed-variable variable-name
  uploadimg(id_f: string, fuser: string) {
    return this.aFS.collection('users').doc(id_f).update(
      {image: fuser, }
    );
  }
}
