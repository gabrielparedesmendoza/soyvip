import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { User } from "../shared/user.class";
import { AlertController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { delay } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //Variable de logeado o no
  public isLogged: any = false;
  hasVerifiedEmail: any = true;
  public guser: any;
  backButtonSubscription = this.platform.backButton.subscribe(() => { });
  contadorbackButtonSubscription: number = 0;
  constructor(
    public afAuth: AngularFireAuth,
    public alertController: AlertController,
    public platform: Platform,
    public router: Router,
  ) {
    afAuth.authState.subscribe(user => (this.isLogged = user));
  }
  async presentAlert(mensaje: string) {
    const alert = await this.alertController.create({
      //header: 'Error',
      //subHeader: 'Informacion',
      message: mensaje,
      buttons: ['OK']
    });

    await alert.present();
  }
  //Salir de la aplicacion
  onClose(nombre: string) {
    switch (nombre) {
      case "category":
        this.backButtonSubscription.unsubscribe();
        this.backButtonSubscription = this.platform.backButton.subscribeWithPriority(999999, () => {
          this.backButtonSubscription.unsubscribe();
          navigator['app'].exitApp();
        });
        break;
      case "brand":
        this.backButtonSubscription.unsubscribe();
        this.backButtonSubscription = this.platform.backButton.subscribe(async () => {
          this.router.navigate(['menu']);
          await delay(1000);
          this.onClose('');
        });
        break;
      default:
        this.backButtonSubscription.unsubscribe();
        this.backButtonSubscription = this.platform.backButton.subscribe(async () => {
          this.router.navigate(['tabs/category']);
          await delay(1000);
          this.onClose('category');
        });
        break;
    }
  }
  //Loging
  async onLogin(user: User) {
    try {
      return await this.afAuth.auth.signInWithEmailAndPassword(user.email, user.password);
      //console.log("Login is succesfull");  

      //if (result) {
      //this.router.navigateByUrl('/home');
      //}  
    } catch (error) {
      console.error(error);
      this.presentAlert(error);
    }
  }

  //Register
  async onRegister(user: User) {
    try {
      return await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password);
      //console.log("Register is succesfull");
    } catch (error) {
      console.error(error);
      this.presentAlert(error);
    }
  }
  //LogOut
  onLogout() {
    //console.log("logout successfull");
    this.afAuth.auth.signOut();
  }

  //Verificaction "Email"
  async  onVerification(juser: User) {

    // try {
    //   return this.afAuth.auth.currentUser.emailVerified;
    //   console.log(this.afAuth.auth.currentUser.emailVerified);

    // } catch (error) {
    //   console.log(error);

    // }


    return this.afAuth.authState.subscribe(user => {
      if (user) {
        juser.hasVerifiedEmail = true;
        juser.hasVerifiedEmail = this.afAuth.auth.currentUser.emailVerified;
        juser.email = user.email;
        window.localStorage['verificacion'] = "SI";
        window.localStorage['email'] = user.email;
        console.log('VerificacionEmail', juser);
      }
      else {
      }
    });


  }


  //Send VerificationEmail
  async onSendVerificationEmail(user: User) {

    console.log("Envio de correo");
    this.afAuth.auth.currentUser.sendEmailVerification();
    user.sentTimestamp = new Date();
    console.log("Fecha" + user.sentTimestamp);
    //this.afAuth.auth.sendPasswordResetEmail(user.email);
  }

  async onSendRestartPass(user: string) {
    this.afAuth.auth.sendPasswordResetEmail(user);
    this.presentAlert("Por favor revise su correo [" + user + "]  para restablecer contrasena");

  }

}

