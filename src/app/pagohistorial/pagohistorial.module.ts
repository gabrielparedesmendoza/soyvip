import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PagohistorialPage } from './pagohistorial.page';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
const routes: Routes = [
  {
    path: '',
    component: PagohistorialPage
  }
];

@NgModule({
  imports: [
    NgxDatatableModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PagohistorialPage]
})
export class PagohistorialPageModule {}
