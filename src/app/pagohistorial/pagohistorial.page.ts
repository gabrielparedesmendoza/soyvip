import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';


@Component({
  selector: 'app-pagohistorial',
  templateUrl: './pagohistorial.page.html',
  styleUrls: ['./pagohistorial.page.scss'],
})

export class PagohistorialPage implements OnInit {
  public historial = [];
  public infouser: any;
  rows = [
    { name: 'Austin', gender: 'Male', company: 'Swimlane' },
    { name: 'Dany', gender: 'Male', company: 'KFC' },
    { name: 'Molly', gender: 'Female', company: 'Burger King' }
  ];
  columns = [{ prop: 'name' }, { name: 'Gender' }, { name: 'Company' }];
  constructor(
    public router: Router,
  ) {
    if(this.router.getCurrentNavigation().extras.state){
      this.infouser = this.router.getCurrentNavigation().extras.state.infouser;
    }
   }

  ngOnInit() {
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < this.infouser[0].coin_Historial.length; i++) {
      const element = this.infouser[0].coin_Historial[i];
      console.log(element);
      this.crearJSON(element);
    }
  }
  crearJSON(actividad: string) {
    // "19/11/2020 16:32:55,GP,Egreso,10"
    let cont = 0;
    const indicador = [];
    for (let i = 0; i < actividad.length; i++) {
      const element = actividad[i];
      if (element === ',') {
        switch (cont) {
        case 0:
          indicador[0] = i;
          break;
        case 1:
          indicador[1] = i;
          break;
        case 2:
          indicador[2] = i;
          break;
        default:
          break;
      }
        cont++;
      }
    }

    // tslint:disable-next-line: variable-name
    const actividad_aux = {
      fecha: actividad.substring(0, 10),
      hora:actividad.substring(11, indicador[0]),
      id: actividad.substring(indicador[0] + 1, indicador[1]),
      estado: actividad.substring(indicador[1] + 1, indicador[2]),
      cantidad: actividad.substring(indicador[2] + 1, actividad.length),
    };
    this.historial.push(actividad_aux);

  }
  closeBrand()
  {
    this.router.navigateByUrl('tabs/user');
  }
}
