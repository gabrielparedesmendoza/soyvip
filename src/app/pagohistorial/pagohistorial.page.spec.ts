import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagohistorialPage } from './pagohistorial.page';

describe('PagohistorialPage', () => {
  let component: PagohistorialPage;
  let fixture: ComponentFixture<PagohistorialPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagohistorialPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagohistorialPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
