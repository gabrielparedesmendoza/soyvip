import { Component, OnInit } from '@angular/core';
import { BrandsService, fuser } from '../services/brands.service';
import { AuthService } from '../services/auth.service';
import { User } from '../shared/user.class';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Platform, PopoverController } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { delay } from 'q';
import { NavigationExtras, Router } from '@angular/router';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { MisociosComponent } from '../components/misocios/misocios.component';
import { PagoComponent } from '../components/pago/pago.component';
import { AlertController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { DatePipe } from '@angular/common';
import { LoadingController } from '@ionic/angular';
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from '@ionic-native/barcode-scanner/ngx';
@Component({
  selector: 'app-user',
  templateUrl: './user.page.html',
  styleUrls: ['./user.page.scss'],
})
export class UserPage implements OnInit {
  hreferal: any;
  public Brands: any = [];
  // tslint:disable-next-line: new-parens
  user: User = new User;
  public infouser: any = [];
  public infouserid: any = [];
  public infouserid2: any = [];
  verificarid = '';
  auxuser = '0';
  infohtlmname = '';
  infohtlmtphone = '';
  infohtlmgenero = '';
  infohtlmimagen = '';
  infohtlmbnombre = '';
  infohtlmbparentezco = '';
  infohtlid = '';
  primernombre = '';
  segundonombre = '';
  primerapellido = '';
  segundoapellido = '';
  infohtlmtphoneb = '';
  infohtlmtcedula = '';
  mensajefdn = 'Seleccionar';
  fechadenacimiento: any;
  selecionar = false;
  auxc: fuser;
  correomisocio: string ='';
  correomisocio2 = '';
  infouserusers = [];
  auxbug = false;
  public mensaje = '';
  public uploadPercent: Observable<number>;
  public downloadUrl: any;
  nombrenuevosocio = '';
  public encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;
  constructor(
    private router: Router,
    private brandservice: BrandsService,
    private authSvc: AuthService,
    private camera: Camera,
    private platform: Platform,
    private file: File,
    private afStorage: AngularFireStorage,
    private clipboard: Clipboard,
    private popoverCtrl: PopoverController,
    private alertCtrl: AlertController,
    private iab: InAppBrowser,
    private datePicker: DatePicker,
    private datePipe: DatePipe,
    private loadingController: LoadingController,
    private barcodeScanner: BarcodeScanner
  ) {
    this.encodeData = 'SoyVIP';
    // Options
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
  }
  scanBar() {
    this.barcodeScanner
      .scan()
      .then(barcodeData => {
        alert("Barcode data " + JSON.stringify(barcodeData));
        this.scannedData = barcodeData;
      })
      .catch(err => {
        console.log("Error", err);
      });
  }

  generateBar() {
    this.encodeData = this.infouser[0].idSoyVIP;
    this.barcodeScanner
      .encode(this.barcodeScanner.Encode.TEXT_TYPE, this.encodeData)
      .then(
        encodedData => {
          console.log(encodedData);
          this.encodeData = encodedData;
        },
        err => {
          console.log('Error occured : ' + err);
        }
      );
  }
  async ngOnInit() {
    this.user.email = window.localStorage.email;
    // this.authSvc.presentAlert(this.user.email);
    this.findUserInfo();
    await delay(2000);
  }
  async verSocios() {
    const popover = await this.popoverCtrl.create({
      component: MisociosComponent,
      componentProps: {
        infouserusers: this.infouserusers,
      },
      translucent: true
    });
    await popover.present();
  }
  async pagar() {
    const popover2 = await this.popoverCtrl.create({
      component: PagoComponent,
      componentProps: {
        infouser: this.infouser
      },
      translucent: true
    });
    await popover2.present();
  }
  async pagarHistorial() {
    // const popover2 = await this.popoverCtrl.create({
    //   component: PagohistorialComponent,
    //   componentProps: {
    //     infouser: this.infouser
    //   },
    //   translucent: true
    // });
    // await popover2.present();
     const navigationExtras: NavigationExtras = {
      state: {
        infouser: this.infouser
      }
    };
     this.router.navigate( ['pagohistorial'], navigationExtras);
  }
  administrar() {
    this.router.navigateByUrl('/administrador');
  }
  doCopyID() {
    this.clipboard.copy(this.infouser[0].idSoyVIP);
    this.authSvc.presentAlert('Tu código VIP ha sido copiado');
  }
  doCopyphonehreferal() {
    this.clipboard.copy('0' + this.infouserid[0].info[1]);
    this.authSvc.presentAlert('El número de tu Socio ha sido copiado');
    console.log();
  }
  openWhatsApp() {
    this.iab.create('https://wa.me/593' + this.infouserid[0].info[1], '_system');
  }
  doRefresh(event) {
    console.log('Begin async operation');

    setTimeout(() => {
      console.log('Async operation has ended');

      // window.location.reload();
      event.target.complete();
    }, 1000);
  }
  onLogout() {
    console.log('logout successfull');
    window.localStorage.verificacion = 'NO'
    window.localStorage.email = '';
    this.authSvc.onLogout();
    setTimeout(() => {
      this.authSvc.onClose('category');
      this.router.navigateByUrl('/login');
    }, 1000);

  }
  reload() {

    window.location.reload();

  }
  async onSendVerificationEmail() {
    const user = await this.authSvc.onSendVerificationEmail(this.user);
  }

  async onVerifyID() {
    // this.hreferal = "SoyVIP";
    this.hreferal = (this.hreferal.replace(' ', '')).toUpperCase();

    if (this.hreferal == '') {
      // this.authSvc.presentAlert("Ingrese el codigo de su parthner");
    } else {

      this.brandservice.getUserInfopartner().subscribe(info => {

        this.infouserid2 = info;

        for (let i = 0; i < info.length; i++) {

          if (this.hreferal === this.infouserid2[i].idSoyVIP || this.hreferal == '') {
            this.infouserid[0] = this.infouserid2[i];
            this.correomisocio = this.infouserid2[i].id;
            this.correomisocio2 = this.infouserid2[i].email;
            this.verificarid = '1';
            break;
          } else {
            this.verificarid = '0';
          }
        }


        switch (this.verificarid) {
          case '0':
            this.mensaje = 'Código de socio no valido';
            this.auxuser = '0';
            break;
          case '1':
            this.mensaje = '';
            this.auxuser = '1';
            console.log('Informacion Parthner', this.infouserid[0]);
            break;
          default:
            this.auxuser = '0';
            break;
        }

      });
    }

  }
  async selectdate() {
    await this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        this.fechadenacimiento = date,
        this.mensajefdn = this.datePipe.transform(date,'dd-MM-yyyy');
      },
      err => console.log('Error occurred while getting date: ', err)
    );

  }
  async createUser() {
    if (
     this.primernombre == '' || this.primernombre == null ||
     this.infohtlmtphone == '' || this.infohtlmtphone == null ||
     this.infohtlmgenero == ''|| this.infohtlmgenero == null
     ) {
      this.authSvc.presentAlert('Por favor ingresar los campos obligatorios');
    } else {
      if (this.primernombre.length < 15) {
        this.authSvc.presentAlert('Ingrese su nombre completo, el que ha ingresado contiene pocos caracteres');
      } else {
        this.infohtlmname = this.primernombre;
        this.infohtlid = this.primernombre.substr(0, 1);
        for (let index = 0; index < this.primernombre.length; index++) {
          if (this.primernombre[index] == ' ') {
            this.infohtlid += this.primernombre.substring(index + 1, index + 2);
          }
        }
        this.infohtlid += (String(this.infohtlmtcedula).substring(6));
        this.infohtlid = this.infohtlid.replace(' ', '').toUpperCase();
        const asdsa: fuser = {
          active: false,
          beneficiario: [this.infohtlmbnombre, this.infohtlmbparentezco, this.infohtlmtphoneb],
          date: new Date,
          id: '',
          email: this.user.email,
          image: this.infohtlmimagen,
          info: [this.infohtlmname, this.infohtlmtphone, this.infohtlmgenero, this.fechadenacimiento, this.infohtlmtcedula],
          referal: this.hreferal,
          referalemail: this.correomisocio2,
          sendimg: true,
          users: [' '],
          idSoyVIP: this.infohtlid,
          admin : false,
          coin_SoyVIP: 0,
          contador_referidos: 0,
          coin_Historial: [' '],
          token: '',
        };
        // console.log(this.fechadenacimiento);

        this.brandservice.getUserInfopartner().subscribe(info => {

          this.infouserid2 = info;

          for (let i = 0; i < info.length; i++) {

            if (this.infohtlid == this.infouserid2[i].idSoyVIP) {
              this.verificarid = '0';
              this.infohtlid = '';
              break;
            } else {
              this.verificarid = '1';
            }
          }
          switch (this.verificarid) {
            case '0':
              this.mensaje = 'Contactese con soporte tecnico, ya que el código de usuario no esta disponible';

              break;
            case '1':
              if (this.auxbug == false) {
                this.auxbug = true;
                // this.authSvc.presentAlert("Felicidades, ahora tienes un ID de SoyVIP, comparteles a tus amigos");
                this.brandservice.uploadInfoUsertoFirebase(this.user.email, asdsa);

                this.brandservice.uploadReferal(this.infouserid[0].email, this.user.email);
              }

              break;

          }
        });

      }
    }

  }
  async openGalery() {
    const loadig = await this.loadingController.create({
      message: ' Cargando...'
    });
    if (this.nombrenuevosocio == '') {
      this.authSvc.presentAlert('Por favor ingrese el correo del nuevo Socio VIP');

    } else {

      if (this.selecionar === false) {
        this.selecionar = true;
        this.nombrenuevosocio = (this.nombrenuevosocio.replace(' ', '')).toUpperCase();

        this.brandservice.getUserInfopartner().subscribe(async info => {

          this.infouserid2 = info;

          for (let i = 0; i < info.length; i++) {

            if (this.nombrenuevosocio === this.infouserid2[i].idSoyVIP) {
              this.infouserid[0] = this.infouserid2[i];
              this.correomisocio = this.infouserid2[i].email;
              this.verificarid = '1';
              break;
            } else {
              this.verificarid = '0';
            }
          }
          switch (this.verificarid) {
            case '0':
              this.authSvc.presentAlert('Usuario no registrado');
              break;
            case '1':

              const option: CameraOptions = {
                quality: 100,
                destinationType: this.camera.DestinationType.FILE_URI,
                sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
                correctOrientation: true
              };
              try {
                await loadig.present();
                const fileUri: string = await this.camera.getPicture(option);
                let file: string;
                if (this.platform.is('ios')) {
                  file = fileUri.split('/').pop();
                } else {
                  file = fileUri.substring(fileUri.lastIndexOf('/') + 1, fileUri.indexOf('?'));
                }
                const path: string = fileUri.substring(0, fileUri.lastIndexOf('/'));
                const buffer: ArrayBuffer = await this.file.readAsArrayBuffer(path, file);
                const blob: Blob = new Blob([buffer], { type: 'image.jpeg' });
                this.uploadPicture(blob);
                await loadig.dismiss();
              } catch (error) {
                this.mensaje = error;
                console.error(error);
                await loadig.dismiss();
              }
              break;
            default:
              this.auxuser = '0';
              break;
          }

        });
        await delay(5000);
        this.selecionar = false;

        // const option: CameraOptions = {
        //   quality: 100,
        //   destinationType: this.camera.DestinationType.FILE_URI,
        //   sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        //   correctOrientation: true
        // }
        // try {
        //   const fileUri: string = await this.camera.getPicture(option);
        //   let file: string;
        //   if (this.platform.is('ios')) {
        //     file = fileUri.split('/').pop();
        //   } else {
        //     file = fileUri.substring(fileUri.lastIndexOf('/') + 1, fileUri.indexOf('?'));
        //   }
        //   const path: string = fileUri.substring(0, fileUri.lastIndexOf('/'));
        //   const buffer: ArrayBuffer = await this.file.readAsArrayBuffer(path, file);
        //   const blob: Blob = new Blob([buffer], { type: "image.jpeg" });
        //   this.uploadPicture(blob);
        // } catch (error) {
        //   this.mensaje = error;
        //   console.error(error);
        // }
      }
    }
  }
  async uploadPicture(blob: Blob) {
    const loadig = await this.loadingController.create({
      message: 'Subiendo...'
    });
    loadig.present();
    const h = new Date().toISOString().substring(0, 7);
    const i = new Date().toISOString().substring(8, 10);
    this.infohtlmimagen = 'user/' + h + '/' + i + '-' + this.user.email + '-' + this.nombrenuevosocio + '.jpg';
    const ref = this.afStorage.ref(this.infohtlmimagen);
    const task = ref.put(blob);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(finalize(() => {
      this.downloadUrl = ref.getDownloadURL();
      loadig.dismiss();
    })).subscribe();


    // task.snapshotChanges().pipe(finalize(() => this.downloadUrl = ref.getDownloadURL())).subscribe();
  }
  async envioPago() {

    // this.onVerifyID();


    const alert = await this.alertCtrl.create({
      header: 'Advertencia',
      subHeader: 'Activando Socio',
      message: 'Los datos ingresados son los correctos?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Estan correctos',
          handler: () => {
            // Activacion de la cuenta
            this.brandservice.activeSocios(this.correomisocio);
            this.authSvc.presentAlert('La cuenta sera activada en un plazo de 24 horas');
            this.brandservice.uploadimg(this.user.email, this.infohtlmimagen);
            this.nombrenuevosocio = '';
            this.downloadUrl = undefined;
          }
        }
      ]
    });
    await alert.present();

  }
  async findUserInfo() {

    this.authSvc.onVerification(this.user);
    await delay(1000);
    // this.user.email="guwaqyh@getnada.com";
    this.brandservice.getUserInfo(this.user.email).subscribe(info => {
      this.infouser = info;
      if (this.infouser[0] == null) {
        const asdsa: fuser = {
          active: false,
          beneficiario: [],
          date: new Date,
          id: '',
          email: '',
          image: '',
          info: [],
          referal: '',
          referalemail: '',
          sendimg: false,
          users: [],
          idSoyVIP: '',
          admin: false,
          coin_SoyVIP: 0,
          contador_referidos: 0,
          coin_Historial: [' '],
          token: '',
        };
        this.infouser[0] = asdsa;
      }
      if (this.infouser[0].coin_SoyVIP === undefined ){
        this.infouser[0].coin_SoyVIP = 0;
      }
      if (this.infouser[0].contador_referidos === undefined ){
        this.infouser[0].contador_referidos = 0;
      }
      // Comprobacion de cuantos dias le quedan
      // var f1= this.infouser[0].date;
      // console.log(this.infouser[0]);

      // var f2 = new Date().getTime();
      // var diasfaltantes=(f2-f1)/(1000*60*60*24);
      // console.log('Faltan estos dias',diasfaltantes);
      //

      // console.log('Informacion Usuario',this.infouser[0]);
      // console.log('Informacion Usuario',this.infouser[0].users.length);

      for (let i = 0; i < this.infouser[0].users.length; i++) {

        this.infouserusers[i] = this.infouser[0].users[i];
        // console.log(this.infouserusers);
      }
      if (this.infouser[0].active == true) {
        this.hreferal = this.infouser[0].referal;
        this.onVerifyID();
      } else {
        this.hreferal = this.infouser[0].referal;
        this.onVerifyID();
      }
    });



  }


}
