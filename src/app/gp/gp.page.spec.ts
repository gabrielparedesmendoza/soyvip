import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpPage } from './gp.page';

describe('GpPage', () => {
  let component: GpPage;
  let fixture: ComponentFixture<GpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
