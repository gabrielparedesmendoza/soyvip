import { Component, OnInit } from '@angular/core';
import { BrandsService } from '../services/brands.service';

@Component({
  selector: 'app-gp',
  templateUrl: './gp.page.html',
  styleUrls: ['./gp.page.scss'],
})
export class GpPage implements OnInit {
  public Brands:any=[];
  constructor( public brandservice:BrandsService ) { }

  ngOnInit() {
    this.brandservice.getInf2("gp").subscribe( brands=> 
      {
          this.Brands =brands; 
          console.log(this.Brands);
          
      }
      );
  }

}
