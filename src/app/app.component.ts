import { Component } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { timer } from 'rxjs/internal/observable/timer';
// import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { tap } from 'rxjs/operators';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  showSplash = true;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private fcm: FCM,
    private toastCtrl: ToastController
    // private firebaseX: FirebaseX,
  ) {
    this.initializeApp();
  }

  async initializeApp() {
    // this.fcm.subscribeToTopic('marketing');
    this.fcm.getToken().then(token => {
      console.log(token);
      window.localStorage.token = token;
      // backend.registerToken(token);
    });
    this.fcm.onNotification().pipe(
      tap(async msg => {
        // tslint:disable-next-line: no-shadowed-variable
        const toast = this.toastCtrl.create({
          message: msg.body,
          duration: 20000,
          cssClass: 'toast-custom-class',
          position: 'top',
          showCloseButton: true,
          closeButtonText: 'Listo',
        }
        );
        (await toast).present();
      }
      )
    ).subscribe();
    // this.fcm.onTokenRefresh().subscribe(token => {
    //   // backend.registerToken(token);
    //   console.log(token);
    // });
    // this.fcm.onNotification().subscribe(data => {
    //   if (data.wasTapped) {
    //     console.log('Received in background');
    //   } else {
    //     console.log('Received in foreground');
    //   }
    // });
    // this.fcm.unsubscribeFromTopic('marketing');

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      setTimeout(() => { this.splashScreen.hide(); }, 200);
      timer(2500).subscribe(() => this.showSplash = false);
    });
  }
}
