import { Component, OnInit } from '@angular/core';
//import { User } from 'src/models/user';
import { Router } from "@angular/router";
//import { AngularFireAuth } from "@angular/fire/auth";
import { User } from "../shared/user.class";
import { AuthService } from "../services/auth.service";
import { AngularFireAuth } from "@angular/fire/auth";
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  splash = true;
  tabBarElement: any;

  constructor(
    public afAuth: AngularFireAuth,
    private authSvc: AuthService,
    private router: Router,
    public alertController: AlertController,
  ) {
    this.tabBarElement = document.querySelector('.tabbar');

  }

  user: User = new User();
  activeanimation: boolean = true;
  ngOnInit() {
    this.authSvc.onClose('category');
    
    setTimeout(() => {
      this.router.navigateByUrl('/login');
      this.activeanimation = false;
    }, 2500);

  }
 
  ionViewDidLoad() {
    this.tabBarElement.style.display = 'none';
    this.splash = false;
    setTimeout(() => {
      this.splash = false;
      this.tabBarElement.style.display = 'flex';
    }, 4000);
  }
  async onLogin() {
    this.user.email=this.user.email.replace(' ', '');
    const user = await this.authSvc.onLogin(this.user);
    if (user) {
      this.router.navigateByUrl("tabs/category");
      console.log("Login successfull");

    }
  }



  // async login(user:User){

  //   const result=this.afAuth.auth.signInWithEmailAndPassword(user.email,user.password);
  //   console.log(result);  
  //   if (result) {
  //     this.router.navigateByUrl('/home');
  //   }

  // }

}
