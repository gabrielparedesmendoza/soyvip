import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children:[
      { path: 'user', loadChildren: '../user/user.module#UserPageModule' },
      { path: 'category', loadChildren: '../category/category.module#CategoryPageModule' },
      { path: 'info', loadChildren: '../info/info.module#InfoPageModule' },
      { path: 'gp', loadChildren: '../gp/gp.module#GpPageModule' },
      
      // { path: 'home', loadChildren: './home/home.module#UserPageModule'},
    ]
  },
  {
    path:'',
    redirectTo:'/tabs/category', 
    pathMatch:'full,'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
