import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { auth } from 'firebase';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {

  constructor(
    private authSvc:AuthService
  ) { }

  ngOnInit() {
  }
  menu1()
  {
    this.authSvc.onClose("category");
  }
  menu2()
  {
    this.authSvc.onClose("user");
  }
  menu3()
  {
    this.authSvc.onClose("");
  }
  menu4()
  {
    this.authSvc.onClose("");
  }
}
