import { Component } from '@angular/core';
import { AuthService } from "../services/auth.service";
import { Router, NavigationExtras } from "@angular/router";
import { User } from "../shared/user.class";

import { NavController, MenuController } from '@ionic/angular';
//import { AngularFireAuth } from "@angular/fire/auth";
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  user: User = new User;
  menu: any;
  
  constructor(
    private router: Router,
    private authSvc: AuthService,
    public nav: NavController,
    private menuCtrl: MenuController

  ) {
    this.user.hasVerifiedEmail = true;
    this.authSvc.onVerification(this.user);
  }

  onLogout() {
    console.log("logout successfull");
    this.authSvc.onLogout();
    this.router.navigateByUrl('/login');
  }
  reload() {
    window.location.reload();
  }
  async onSendVerificationEmail() {
    const user = await this.authSvc.onSendVerificationEmail(this.user);
  }
  selectMenu1() {
    // tslint:disable-next-line: no-string-literal
    const menu = this.menu = "category01";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu2() {
    const menu = this.menu = "category02";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu3() {
    const menu = this.menu = "category03";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu4() {
    const menu = this.menu = "category04";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu5() {
    const menu = this.menu = "category05";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu6() {
    const menu = this.menu = "category06";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu7() {
    const menu = this.menu = "category07";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu8() {
    const menu = this.menu = "category08";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu9() {
    const menu = this.menu = "category09";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu10() {
    const menu = this.menu = "category10";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu11() {
    const menu = this.menu = "category11";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }
  selectMenu12() {
    const menu = this.menu = "category12";
    let navigationExtras: NavigationExtras = {
      state: {
        menu: this.menu
      }
    }
    this.router.navigate(['menu'], navigationExtras);
  }


  

}
