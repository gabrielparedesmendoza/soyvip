import { Component, OnInit } from '@angular/core';
import { BrandsService, brand } from '../services/brands.service';
import { ModalController } from '@ionic/angular';
import { BrandComponent } from '../components/brand/brand.component';
import { Router} from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
public Brands: any = [];
public menu: any;
constructor(
    public brandservice: BrandsService,
    private modal: ModalController,
    public router: Router,
    ) {
      if (this.router.getCurrentNavigation().extras.state) {
        this.menu = this.router.getCurrentNavigation().extras.state.menu;
      }
     }
  ngOnInit() {
    this.brandservice.getInf(this.menu).subscribe( brands => {
          this.Brands = brands;
      }
      );
  }
  // tslint:disable-next-line: no-shadowed-variable
  openBrand(brand: any) {
    this.modal.create({
      component: BrandComponent,
      componentProps: {
        brand
      }
    }).then((modal) => modal.present());
  }

}
