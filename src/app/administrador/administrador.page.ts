import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { BrandsService, brand } from '../services/brands.service';
import { PagohistorialComponent } from '../components/pagohistorial/pagohistorial.component';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.page.html',
  styleUrls: ['./administrador.page.scss'],
})
export class AdministradorPage implements OnInit {
  public Brands = [];
  public marcaSeleccionada = 'category01';
  constructor(private brandservice: BrandsService,
              private popoverCtrl: PopoverController,
              private router: Router, ) { }

  ngOnInit() {
    this.brandservice.getInfAdmin(this.marcaSeleccionada).subscribe( brands => {
      this.Brands = brands;
  }
  );
  }
  OnChange(event) {
    this.brandservice.getInfAdmin(this.marcaSeleccionada).subscribe( brands => {
          this.Brands = brands;
      }
      );
  }
  async agregarCiudad() {
    const popover2 = await this.popoverCtrl.create({
      component: PagohistorialComponent,
      // componentProps: {
      //   infouser: this.infouser
      // },
      translucent: true
    });
    await popover2.present();
  }
  nuevaPublicidad() {
    const navigationExtras: NavigationExtras = {
      state: {
        publicidadFlag: true
      }
    };
    this.router.navigate( ['detalles'], navigationExtras);
  }
}
