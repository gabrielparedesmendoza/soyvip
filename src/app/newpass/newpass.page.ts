import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-newpass',
  templateUrl: './newpass.page.html',
  styleUrls: ['./newpass.page.scss'],
})
export class NewpassPage implements OnInit {
email:string="";
  constructor(public authSv:AuthService) { }

  ngOnInit() {
    
  }
  onRestablecerContra()
  {
    this.authSv.onSendRestartPass(this.email);
  }
}
