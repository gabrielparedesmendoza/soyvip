import { Component, OnInit } from '@angular/core';
import { BrandsService, brand } from '../services/brands.service';
import { ActivatedRoute } from '@angular/router';
import { NavController, LoadingController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Platform } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.page.html',
  styleUrls: ['./detalles.page.scss'],
})

export class DetallesPage implements OnInit {
  publicidadFlag: any;
  public  publicidad: any = [];
  brandId = null;
  infohtlmimagen = '';
  idArray = [];
  marcaSeleccionada = 'category01';
  imagenesArray = ['', '', '', '', '', '', ''];
  ciudades: [];
  ciudadesJSON: any[] = [] ;
  ciudadSelecionada = 'Manta';
  customActionSheetOptions: any = {
    header: 'Ciudad',
    subHeader: 'Seleccione una ciudad'
  };
  brands: brand = {
    description: '',
    name: '',
    id: '',
    image: '',
    activity1: [],
    activity2: [],
    activity3: [],
    activity4: [],
    activity5: [],
    info: ['', '', '', '', ''],
  };
  
  // tslint:disable-next-line: max-line-length
  constructor(
    private route: ActivatedRoute,
    private nav: NavController,
    private brandservice: BrandsService,
    private loadingController: LoadingController,
    private camera: Camera,
    private platform: Platform,
    private file: File,
    private afStorage: AngularFireStorage,
    private router: Router,
    private alertController: AlertController,
    ) {
      if (this.router.getCurrentNavigation().extras.state) {
        this.publicidadFlag = this.router.getCurrentNavigation().extras.state.publicidadFlag;
      }
  }

  ngOnInit() {
      this.encontrarCiudades();
      this.brandId = this.route.snapshot.params.id;
      if (this.brandId) {
        this.loadBrand();
        this.idArray = this.brandId.split('/');
      } else if (this.publicidadFlag) {
        this.encontrarPublicidad();
      }
  }
  async encontrarPublicidad() {
    this.brandservice.getPublicidad().subscribe( publicidad => {

          this.brands.activity1[1] = publicidad[0].url;
          this.brands.activity2[1] = publicidad[1].url;
          this.brands.activity3[1] = publicidad[2].url;
          this.brands.activity4[1] = publicidad[3].url;
          this.brands.activity5[1] = publicidad[4].url;
          this.brands.activity1[0] = publicidad[5].url;
          this.brands.activity2[0] = publicidad[6].url;
          this.brands.activity3[0] = publicidad[7].url;
          this.brands.activity4[0] = publicidad[8].url;
          this.brands.activity5[0] = publicidad[9].url;
      }
      );
  }
  async loadBrand() {
    const loadig = await this.loadingController.create({
      message: 'Cargando...'
    });
    await loadig.present();
    this.brandservice.getBrand(this.brandId).subscribe( brands => {
      loadig.dismiss();
      brands.forEach(element => {
        if (element != null) {
          this.brands = element;
        }
      });
      console.log(this.brands);
    }
    );
  }
  async encontrarCiudades() {
    // tslint:disable-next-line: variable-name
    this.brandservice.getCiudad().subscribe( a_global => {
          this.ciudades = a_global.ciudades;
          // console.log(this.ciudades);
          for (const item of this.ciudades) {
            const aux = {
              nombre: item,
            };
            this.ciudadesJSON.push(aux);
          }
          // console.log(this.ciudadesJSON);
        }
      );

    }
  subirImagen0() {
    this.openGallery(0);
  }
  subirImagen1() {
    this.openGallery(1);
  }
  subirImagen2() {
    this.openGallery(2);
  }
  subirImagen3() {
    this.openGallery(3);
  }
  subirImagen4() {
    this.openGallery(4);
  }
  subirImagen5() {
    this.openGallery(5);
  }
  subirImagen6() {
    this.openGallery(6);
  }
  eliminarImagen0() {
    this.brands.image = '';
  }
  eliminarImagen1() {
    this.brands.info[3] = '';
  }
  eliminarImagen2() {
    this.brands.activity1[1] = '';
  }
  eliminarImagen3() {
    this.brands.activity2[1] = '';
  }
  eliminarImagen4() {
    this.brands.activity3[1] = '';
  }
  eliminarImagen5() {
    this.brands.activity4[1] = '';
  }
  eliminarImagen6() {
    this.brands.activity5[1] = '';
  }
  async openGallery(nombreImagen: number) {
    const option: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      correctOrientation: true
    };
    try {
      const fileUri: string = await this.camera.getPicture(option);
      let file: string;
      if (this.platform.is('ios')) {
        file = fileUri.split('/').pop();
      } else {
        file = fileUri.substring(fileUri.lastIndexOf('/') + 1, fileUri.indexOf('?'));
      }
      const path: string = fileUri.substring(0, fileUri.lastIndexOf('/'));
      const buffer: ArrayBuffer = await this.file.readAsArrayBuffer(path, file);
      const blob: Blob = new Blob([buffer], { type: 'image.jpeg' });
      this.uploadPicture(blob, nombreImagen);
    } catch (error) {
      console.error(error);
    }
  }
  async uploadPicture(blob: Blob, nombreImagen: number) {
    const loadig = await this.loadingController.create({
      message:'Cargando...'
    });
    await loadig.present();
    this.infohtlmimagen = 'categories/' + this.brandId + '/image' + nombreImagen + '.jpg';
    const ref = this.afStorage.ref(this.infohtlmimagen);
    const task = ref.put(blob);
    // this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(finalize(() => {
      ref.getDownloadURL().subscribe(url => {
        this.imagenesArray[nombreImagen] = url;
        switch (nombreImagen) {
          case 0:
            this.brands.image = this.imagenesArray[0];
            break;
          case 1:
            this.brands.info[0] = '';
            this.brands.info[1] = '';
            this.brands.info[2] = '';
            this.brands.info[3] = this.imagenesArray[1];
            break;
          case 2:
            this.brands.activity1[0] = '';
            this.brands.activity1[1] = this.imagenesArray[2];
            break;
          case 3:
            this.brands.activity2[0] = '';
            this.brands.activity2[1] = this.imagenesArray[3];
            break;
          case 4:
            this.brands.activity3[0] = '';
            this.brands.activity3[1] = this.imagenesArray[4];
            break;
          case 5:
            this.brands.activity4[0] = '';
            this.brands.activity4[1] = this.imagenesArray[5];
            break;
          case 6:
            this.brands.activity5[0] = '';
            this.brands.activity5[1] = this.imagenesArray[6];
            break;
          default:
            break;
        }
        console.log('My ImageUrl' + this.imagenesArray[nombreImagen]);
      });
      loadig.dismiss();
    })).subscribe();
  }
  async guardarMarca() {
    this.brands.info[4] = this.ciudadSelecionada;
    const loadig = await this.loadingController.create({
      message: 'Guardando...'
    });
    await loadig.present();
    if (this.publicidadFlag) {
      this.publicidad[0] = this.brands.activity1[1];
      this.publicidad[1] = this.brands.activity2[1];
      this.publicidad[2] = this.brands.activity3[1];
      this.publicidad[3] = this.brands.activity4[1];
      this.publicidad[4] = this.brands.activity5[1];
      this.publicidad[5] = this.brands.activity1[0];
      this.publicidad[6] = this.brands.activity2[0];
      this.publicidad[7] = this.brands.activity3[0];
      this.publicidad[8] = this.brands.activity4[0];
      this.publicidad[9] = this.brands.activity5[0];
      console.log(this.publicidad);
      await this.brandservice.actualizarPublicidad(this.publicidad);
    } else {
      if (this.brandId) {
        await this.brandservice.actualizarMarca(this.idArray[0], this.idArray[1], this.brands);
      } else {
        await this.brandservice.agregarMarca(this.brands, this.marcaSeleccionada);
      }
    }
    loadig.dismiss();
    this.router.navigateByUrl('/administrador');
  }
  async eliminarMarca() {
    const alert = await this.alertController.create({
      header: 'Confirmación!',
      message: 'Estas seguro que deseas <strong>borrar</strong> la marca',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Borrar',
          handler: () => {
            console.log('Confirm Okay');
            this.brandservice.eliminarMarca(this.idArray[0], this.idArray[1]);
            this.router.navigateByUrl('/administrador');
          }
        }
      ]
    });

    await alert.present();
  }
}
