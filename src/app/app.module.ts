import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule, FirestoreSettingsToken } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrandComponent } from './components/brand/brand.component';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { File } from '@ionic-native/file/ngx';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import { MisociosComponent } from './components/misocios/misocios.component';
import { PagoComponent } from './components/pago/pago.component';
import { PagohistorialComponent } from './components/pagohistorial/pagohistorial.component';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { DatePipe } from '@angular/common';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
// import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { FCM } from '@ionic-native/fcm/ngx';

@NgModule({
  declarations: [AppComponent, BrandComponent, MisociosComponent, PagoComponent, PagohistorialComponent,],
  entryComponents: [BrandComponent, MisociosComponent, PagoComponent, PagohistorialComponent],
  imports: [BrowserModule,
    NgxDatatableModule,
    FormsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp({
      apiKey: 'AIzaSyAPrsiHM7UjyHg-KxGXE5rLj-0i4k2FGHk',
    authDomain: 'soyvip-9c183.firebaseapp.com',
    databaseURL: 'https://soyvip-9c183.firebaseio.com',
    projectId: 'soyvip-9c183',
    storageBucket: 'soyvip-9c183.appspot.com',
    messagingSenderId: '20474624903',
    appId: '1:20474624903:web:e7a063d0e8dd3c7d91f122'
    }),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
  ],
  providers: [
    BarcodeScanner,
    // FirebaseX,
    FCM,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    InAppBrowser,
    Camera,
    Clipboard,
    File,
    DatePicker,
    DatePipe,
    {provide: FirestoreSettingsToken, useValue: {
    }}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
