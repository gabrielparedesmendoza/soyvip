import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class Auth2Guard implements CanActivate {
  constructor(private router:Router,private authSvc:AuthService){  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log('Almacenamiento interno',JSON.stringify(window.localStorage['verificacion']));
      if (this.authSvc.isLogged || window.localStorage['verificacion']=="SI") {
      console.log("Access permitido");
      this.router.navigateByUrl('/tabs/category');
      return false;
      
    } else {
      console.log("Access denied");
      return true;
    }
    //  false protege;
  }
  

}
